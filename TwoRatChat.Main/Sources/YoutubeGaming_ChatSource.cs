﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Timers;
using System.Web;
using System.Windows.Threading;
using System.Xml.Linq;
using TwoRatChat.Controls;

namespace TwoRatChat.Main.Sources {
    public class YoutubeGaming_ChatSource: TwoRatChat.Model.ChatSource {
        int _sequence = 1;

        protected virtual string youTubeCommentUri {
            get {
                return "https://www.youtube.com/live_comments?action_get_comments=1&video_id={0}&lt={1}&format=proto&pd=30000&rc={2}&scr=true&comment_version=1";
            }
        }

        protected virtual string youTubeViewersUri {
            get {
                return "https://gaming.youtube.com/live_stats?v={0}&t={1}";
            }
        }

        Timer _retriveTimer;
        List<Example.Message> _cache;
        string _id;
        bool _showComments = false;

        public override string Id { get { return "youtube"; } }

        public YoutubeGaming_ChatSource( Dispatcher dispatcher )
            : base(dispatcher) {
        }

        void _retriveTimer_Tick( object sender, EventArgs e ) {
            _retriveTimer.Stop();
            loadChat();
        }

        bool _inUpdateViewers = false;
        public override void UpdateViewerCount() {
            if (!_inUpdateViewers ) {
                WebClient wc = new WebClient();
                wc.DownloadStringCompleted += new DownloadStringCompletedEventHandler(( b, a ) => {
                    if (a.Error == null) {
                        int h;
                        if ( int.TryParse( a.Result, out h ) ) {
                            if ( h == 0 ) {
                                this.ViewersCount = 0;
                                this.Header = "";
                            } else {
                                this.ViewersCount = h;
                                this.Header = h.ToString();
                            }
                        }
                    }
                    _inUpdateViewers = false;
                });

                _inUpdateViewers = true;
                string u = string.Format( youTubeViewersUri, this.Uri, DateTime.Now.Millisecond );
                wc.DownloadStringAsync(new Uri(
                    u, UriKind.RelativeOrAbsolute));
            }
        }

        private class MyWebClient : WebClient {
            protected override WebRequest GetWebRequest( Uri uri ) {
                WebRequest w = base.GetWebRequest(uri);
                w.Timeout = 1000;
                return w;
            }
        }

        Random r = new Random();
        CookieAwareWebClient wc;

        long _lastDt;

        void loadChat() {
            if (wc == null) {
                wc = new CookieAwareWebClient(new CookieContainer()) { Encoding = Encoding.UTF8 };
                wc.Headers.Add("Cache-Control", "max-age=0");
             //   wc.Headers.Add("user-agent", "TwoRatChat");
                wc.DownloadDataCompleted += ( a, b ) => {
                    this.Status = true;

                    if (b.Error == null) {
                        
                        try {
                            XElement x = XElement.Parse( Encoding.UTF8.GetString( b.Result ) );
                            var yy = x.Element( "comments" ).Value.Replace( "-", "+" ).Replace( "_", "/" );
                            var xx = Convert.FromBase64String( yy );
                            var m = Example.Messages.Deserialize( xx );


                            _lastDt = long.Parse( x.Element( "latest_time" ).Value );
                            if ( m.Mmm.Count > 0) {
                                updateMessages( m.Mmm );
                                //_lastDt = m.Mmm.Last().Date;
                            }else {

                            }
                        } catch ( Exception er) {
                            Console.Write(er.Message);
                        }
                    } else {
                    }

                    if (_retriveTimer != null)
                        _retriveTimer.Start();

                    UpdateViewerCount();

                };

            }
            var uu = string.Format( youTubeCommentUri, this.Uri, _lastDt, _sequence++ );
            wc.DownloadDataAsync( new Uri( uu, UriKind.Absolute ) );
        }

        Example.Message getById( string id ) {
            for (int j = 0; j < _cache.Count; ++j)
                if (_cache[j].Id == id)
                    return _cache[j];
            return null;
        }

        void updateMessages(IEnumerable<Example.Message> mmm) {
            for ( int j = 0; j < _cache.Count; ++j )
                _cache[j].NeedToDelete--;

            //var msg = (from b in msgs
            //           orderby b.Date
            //           select b).ToArray();

            List<Example.Message> NewMessage = new List<Example.Message>();
            ////////////
            foreach ( var m in mmm ) {
                Example.Message oldM = getById( m.Id );

                if ( oldM == null ) {
                    _cache.Add( m );
                    NewMessage.Add( m );
                    m.NeedToDelete = 60 * 5;
                } else {
                    m.NeedToDelete = 60 * 5;
                }
            }
            /////////////

            int i = 0;
            while ( i < _cache.Count )
                if ( _cache[i].NeedToDelete < 0 )
                    _cache.RemoveAt( i );
                else
                    ++i;

            if ( _showComments ) {
                if ( NewMessage.Count > 0 ) {
                    newMessagesArrived( from b in NewMessage
                                        orderby b.Date
                                        select new TwoRatChat.Model.ChatMessage( getBages( b ) ) {
                                            Date = DateTime.Now,
                                            Name = b.User.Nick,
                                            Text = HttpUtility.HtmlDecode( b.Text ),
                                            Source = this,
                                            Id = _id,
                                            ToMe = this.ContainKeywords( b.Text ),

                                            //Form = 0
                                        } );
                }

            }

            _showComments = true;
        }


        Uri[] getBages(Example.Message m) {
            if( Properties.Settings.Default.youtube_ShowIcons ) {
                return new System.Uri[] { new Uri( m.User.Url, UriKind.Absolute ) };
            }
            return new System.Uri[0];
        }


        //eWRei_9cEO8
        public override void Create( string streamerUri, string id ) {
            this.Label = this._id = id;
            this.Uri = this.SetKeywords( streamerUri, false );
            this.Tooltip = "youtube";

            _showComments = Properties.Settings.Default.Chat_LoadHistory;

            _cache = new List<Example.Message>();

            WebClient wc = new WebClient();
            try {
                wc.DownloadString( new Uri( string.Format( youTubeCommentUri, this.Uri, _lastDt, _sequence ), UriKind.Absolute ) );
            } catch ( WebException we ) {
                if ( ((HttpWebResponse)we.Response).StatusCode == HttpStatusCode.NotFound )
                    fireOnFatalError( FatalErrorCodeEnum.ChannelNotFound );
                if ( ((HttpWebResponse)we.Response).StatusCode == HttpStatusCode.BadRequest )
                    fireOnFatalError( FatalErrorCodeEnum.ChannelNotFound );
                return;
            } catch ( Exception err ) {
                App.Log( '?', "Error while create gipsyteam object: {0}", err );
                fireOnFatalError( FatalErrorCodeEnum.ParsingError );
                return;
            }

            _retriveTimer = new Timer();
            _retriveTimer.Interval = 1000;// TimeSpan.FromSeconds(5.0);
            _retriveTimer.Elapsed += _retriveTimer_Tick;

            loadChat();
        }


        public override void Destroy() {
            if( _retriveTimer != null )
                _retriveTimer.Stop();
        }
    }
}
