﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Timers;
using System.Web;
using System.Windows.Threading;
using System.Xml.Linq;
using TwoRatChat.Controls;

namespace TwoRatChat.Main.Sources {
    public class Youtube_ChatSource : YoutubeGaming_ChatSource {
        public Youtube_ChatSource(Dispatcher dispatcher)
            : base(dispatcher) {
        }

        protected override string youTubeViewersUri {
            get {
                return "https://www.youtube.com/live_stats?v={0}&t={1}"; ;
            }
        }
    }
}